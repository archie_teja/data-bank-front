
angular.module('subjectService',[])

    .factory('Subject', function($http) {

        return{
            //get all the subjects
            get : function(){
                return $http.get('http://localhost:8000/subjects');
            },

            //save a subject
            save : function(subjectData){
                return $http({
                    method:'POST',
                    url:'http://localhost:8000/subjects',
                    //headers: { 'Authorization' : 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMDBcL2F1dGgiLCJpYXQiOiIxNDM4NTc5MTg4IiwiZXhwIjoiMTQzODU4Mjc4OCIsIm5iZiI6IjE0Mzg1NzkxODgiLCJqdGkiOiJhZTEwM2E4OWUyMGQ0ZjM3YjY2NWI5MjZjMjE2YTE1NiJ9.Gj-yHy4dh-_6GDZEthxzNLAMxWsu3HDPSdYlW7YOSqI' },
                    //data: $.param(subjectData)
                    data:JSON.stringify(subjectData)
                });

            },

            //destroy a comment
            destroy : function(id){
                return $http.delete('http://localhost:8000/subjects/' + id);
            },

            //get subject details and questions
            getSubject : function(id){
                return $http.get('http://localhost:8000/subjects/' + id);
            }
        }
    });