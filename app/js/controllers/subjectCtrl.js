angular.module('subjectCtrl', [])


//inject the subject service into our controller
    .controller('subjectController', function($scope, $http, Subject) {
        //object to hold all the data for the new subject subject form
        $scope.subjectData={};

        //loading variable to show the spinning loading icon
        $scope.loading = true;

        //get all the subjects and bind it to the $scope.subject object
        //use the function we created in our services
        //get all subjects============
        Subject.get()
            .success(function(data){
                $scope.subjects = data;
                $scope.loading =false;
            });

        //function to handle submitting the form
        //save all subject =========
        $scope.submitSubject = function(){
            $scope.loading = true;

            //save the subject. Pass in subject data from the form
            //use th function we created in our service
            Subject.save($scope.subjectData)
                .success(function(data){
                    //if successful, we'll need to refresh the subject list
                    Subject.get()
                        .success(function(getData){
                            $scope.subjects = getData;
                            $scope.loading = false;
                        });
                })
                .error(function (data) {
                    console.log(data);
                })
        };

        //function to handle deleting a subject
        //delete a subject
        $scope.deleteSubject = function(id){
            $scope.loading = true;

            var response = confirm("Are you sure?");
            if(response == true){
                //use the function we created in our service
                Subject.destroy(id)
                    .success(function(data){

                        //if successful, we'll need to refresh the subject list
                        Subject.get()
                            .success(function(getData){
                                $scope.subjects = getData;
                                $scope.loading = false;
                            });

                    });
            }

        };

        $scope.selectSubject = function(id){
            $scope.loading = true;

            Subject.getSubject(id)
                .success(function(getData){
                    $scope.subjectDetails = getData;
                    $scope.loading = false;
                });
        }


    });