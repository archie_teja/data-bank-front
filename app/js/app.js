var dataBankApp = angular.module('dataBankApp', ['ngRoute','subjectCtrl', 'subjectService'/*, 'questionCtrl', 'questionService'*/]);

dataBankApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/home', {
                templateUrl: 'partials/home.php'
            }).
            when('/subjects', {
                templateUrl: 'partials/subject-list.php',
                controller: 'subjectController'
            }).
            when('/subject/:subjectId',{
                templateUrl:'partials/question-list.php',
                controller: 'subjectController'
            }).
            otherwise({
                redirectTo: '/subjects'
            });
    }]);