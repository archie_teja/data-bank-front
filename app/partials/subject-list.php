
    <div class="col-md-8 col-md-offset-2">


        <!-- NEW SUBJECT FORM =============================================== -->
        <form ng-submit="submitSubject()"> <!-- ng-submit will disable the default form action and use our function -->

            <!-- AUTHOR -->
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="name" ng-model="subjectData.name" placeholder="Find or Add Subject">
            </div>

            <!-- SUBMIT BUTTON -->
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
            </div>
        </form>

        <!-- LOADING ICON =============================================== -->
        <!-- show loading icon if the loading variable is set to true -->
        <p class="text-center" ng-show="loading"><span class="fa fa-spinner fa-5x fa-spin"></span></p>

        <!-- THE SUBJECTS =============================================== -->
        <!-- hide these subjects if the loading variable is true -->
        <div class="subject" ng-hide="loading" ng-repeat="subject in subjects | filter:subjectData.name" >

            <h1>{{ subject.name }}</h1>

            <p> <a href="#" ng-click="editSubject(subject.id)" class="text-muted">Edit</a>  <a href="#" ng-click="deleteSubject(subject.id)" class="text-muted">Delete</a> </p>
        </div>

    </div>
